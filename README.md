# Wardrobify

Team:

* Person 1 - Samantha - shoes
* Suzanne Bisht - Hats

## Design

## Shoes microservice

I don't understand any of this. I'm doing a Udemy class instead of finishing this.

## Hats microservice

The backend of the hat microservice was developed using Django, involving three key files: api_urls.py, models.py, and api_views.py. The api_urls.py file created two paths to route functions in api_view.py, allowing for listing and adding new hats, as well as displaying hat details based on their unique id. In models.py, two models were defined: "Hats" and "LocationVO," with the latter receiving data from the main Wardrobe API through polling. The api_views.py file contained two functions, "api_list_hats" and "api_show_hats," and utilized three encoders to process data. The frontend was built using React, where App.js was modified to add routes for "hats," HatsList.js displayed hat details in a table and supported hat deletion, and HatForm.js handled hat addition via a form. Additionally, poller.py was developed to control the polling of data from Wardrobe to the Hats microservice, enabling updates to LocationVO data displayed in the table.
