from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import ShoePair, ShoePairEncoder, BinVO




require_http_methods(["GET", "POST"])
def list_shoe_pairs(request):
    if request.method == "GET":
        shoes = ShoePair.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoePairEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            shoepair_href = content["location"]
            location = BinVO.objects.get(id=content["location"])
            content["location"] = location
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoepair id"},
                status=400,
            )

        shoe = ShoePair.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoePairEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def shoe_details (request, id):
    if request.method == "GET":
        shoes = ShoePair.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoePairEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = BinVO.objects.get(id=content["location"])
                content["location"] = location
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        ShoePair.objects.filter(id=id).update(**content)
        shoes = ShoePair.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoePairEncoder,
            safe=False,
        )
    else:
        count, _ = ShoePair.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
