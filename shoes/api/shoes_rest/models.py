from django.db import models
from api.common.json import ModelEncoder
import json
from django.http import JsonResponse

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


class ShoePair(models.Model):
    manufacturer = models.TextField()
    shoe_model_name = models.TextField()
    shoe_color = models.TextField()
    shoe_photo_url = models.URLField()

    location = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.CASCADE,
        null=True,
    )


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["id", "closet_name", "bin_number", "bin_size", "import_href"]



class ShoePairEncoder(ModelEncoder):
    model = ShoePair
    properties = [
        "id",
        "manufacturer",
        "shoe_model_name",
        "shoe_color",
        "shoe_photo_url",
        "location",
    ]
    encoders = {
        "location": BinVOEncoder(),
    }
