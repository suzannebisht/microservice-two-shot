from django.urls import path
from .views import list_shoe_pairs,shoe_details


urlpatterns = [
    path("shoes/", list_shoe_pairs, name="list_of_shoes"),
    path("shoes/<int:id>", shoe_details, name="shoe_details"),
]
